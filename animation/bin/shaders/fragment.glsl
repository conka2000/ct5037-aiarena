#version 330

in vec4 vNormal;
in vec4 vColour;
in vec2 vUV;

out vec4 FragColor;

uniform vec4 cameraPosition;
uniform vec4 lightDirection;

uniform sampler2D DiffuseTexture;
 
void main()
{
	float nDl = max(0.f, dot(vNormal, -lightDirection));
	FragColor =  vColour * nDl;
}
