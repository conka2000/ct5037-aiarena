#version 330

in vec4 Position;
in vec4 Colour;
in vec4 Normal;
in vec4 Indices;
in vec4 Weights;
in vec2 Tex1;

out vec4 vNormal;
out vec4 vColour; 
out vec2 vUV;

uniform mat4 ProjectionView; 
uniform mat4 Model;

// we need to give our bone array a limit
const int MAX_BONES = 128;

uniform mat4 Bones[MAX_BONES];

void main() 
{ 
	// cast the indices to integer's so they can index an array
	ivec4 index = ivec4(Indices);

	// sample bones and blend up to 4
	vec4 P = vec4(0.f, 0.f, 0.f, 1.f);

	P =  Bones[index.x] * Position * Weights.x;
	P += Bones[index.y] * Position * Weights.y;
	P += Bones[index.z] * Position * Weights.z;
	P += Bones[index.w] * Position * Weights.w;

	vec4 N = vec4(0.f, 0.f, 0.f, 0.f);
	N =  Bones[index.x] * Normal * Weights.x;
	N += Bones[index.y] * Normal * Weights.y;
	N += Bones[index.z] * Normal * Weights.z;
	N += Bones[index.w] * Normal * Weights.w;
	

	vColour = Colour;
	vUV = Tex1;
	vNormal = normalize(Model * N);

	gl_Position = ProjectionView * Model * P;

}