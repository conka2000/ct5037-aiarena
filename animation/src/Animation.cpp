#include "Animation.h"
#include "Gizmos.h"
#include "Utilities.h"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/ext.hpp>
#include <iostream>

using namespace std;


#define DEFAULT_SCREENWIDTH 1280
#define DEFAULT_SCREENHEIGHT 720

// s1504719 - Cameron Seeley
Animation::Animation()
{

}

Animation::~Animation()
{

}

bool Animation::onCreate()
{
	// initialise the Gizmos helper class
	Gizmos::create();

	//Load the shaders for this program
	m_vertexShader = Utility::loadShader("./shaders/vertex.glsl", GL_VERTEX_SHADER);
	m_fragmentShader = Utility::loadShader("./shaders/fragment.glsl", GL_FRAGMENT_SHADER);
	//Define the input and output varialbes in the shaders
	//Note: these names are taken from the glsl files -- added in inputs for UV coordinates
	const char* szInputs[] = { "Position", "Colour", "Normal", "Indices", "Weights","Tex1" };
	const char* szOutputs[] = { "FragColor" };
	//bind the shaders to create our shader program
	m_programID = Utility::createProgram(
		m_vertexShader,
		0,
		0,
		0,
		m_fragmentShader,
		6, szInputs, 1, szOutputs);


	//m_fbxModel = new FBXFile();
	//m_fbxModel->load("./models/UE4Man_Anim_rotationcheck.FBX", FBXFile::UNITS_METER);

	//m_fbxModel2 = new FBXFile();
	//m_fbxModel2->load("./models/UE4Man_Anim_rotationcheck.FBX", FBXFile::UNITS_METER);

	//Generate our OpenGL Vertex and Index Buffers for rendering our FBX Model Data
	// OPENGL: genorate the VBO, IBO and VAO
	glGenBuffers(1, &m_vbo);
	glGenBuffers(1, &m_ibo);
	glGenVertexArrays(1, &m_vao);

	// OPENGL: Bind  VAO, and then bind the VBO and IBO to the VAO
	glBindVertexArray(m_vao);
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);

	//There is no need to populate the vbo & ibo buffers with any data at this stage
	//this can be done when rendering each mesh component of the FBX model

	// enable the attribute locations that will be used on our shaders
	glEnableVertexAttribArray(0); //Pos
	glEnableVertexAttribArray(1); //Col
	glEnableVertexAttribArray(2); //Norm
	glEnableVertexAttribArray(3); //Indices
	glEnableVertexAttribArray(4); //Weights
	glEnableVertexAttribArray(5); //Tex1


								  // tell our shaders where the information within our buffers lie
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), ((char *)0) + FBXVertex::PositionOffset);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_TRUE, sizeof(FBXVertex), ((char *)0) + FBXVertex::ColourOffset);
	glVertexAttribPointer(2, 4, GL_FLOAT, GL_TRUE, sizeof(FBXVertex), ((char *)0) + FBXVertex::NormalOffset);
	glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), ((char *)0) + FBXVertex::IndicesOffset);
	glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), ((char *)0) + FBXVertex::WeightsOffset);
	glVertexAttribPointer(5, 2, GL_FLOAT, GL_TRUE, sizeof(FBXVertex), ((char *)0) + FBXVertex::TexCoord1Offset);


	// finally, where done describing our mesh to the shader
	// we can describe the next mesh
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// create a world-space matrix for a camera
	m_cameraMatrix = glm::inverse(glm::lookAt(glm::vec3(10, 10, 10), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0)));

	// create a perspective projection matrix with a 90 degree field-of-view and widescreen aspect ratio
	m_projectionMatrix = glm::perspective(glm::pi<float>() * 0.25f, DEFAULT_SCREENWIDTH / (float)DEFAULT_SCREENHEIGHT, 0.1f, 1000.0f);

	/*m_modelMatrix = glm::mat4();

	m_modelMatrix[3] = glm::vec4(0.f, 0.f, 5.f, 1.f);

	m_modelMatrix2 = glm::mat4();

	m_modelMatrix2[3] = glm::vec4(0.f, 0.f, -5.f, 1.f);*/

	//m_modelMatrix2 = glm::rotate(m_modelMatrix, 0.015f, glm::vec3(0.f, 180.f, 0.f));

	// set the clear colour and enable depth testing and backface culling
	glClearColor(0.25f, 0.25f, 0.25f, 1.f);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	// Give all actor*s an actor with specific name
	red1 = new Red("Red-1");
	red2 = new Red("Red-2");
	red3 = new Red("Red-3");
	red4 = new Red("Red-4");

	yellow1 = new Yellow("Yellow-1");

	purple1 = new Purple("Purple-1");
	purple2 = new Purple("Purple-2");

	green1 = new Green("Green-1");
	green2 = new Green("Green-2");

	blue1 = new Blue("Blue-1");
	blue2 = new Blue("Blue-2");

	// Start all actors with different seed each
	red1->Start(time(NULL));
	red2->Start(time(NULL) * 2);
	red3->Start(time(NULL) * 3);
	red4->Start(time(NULL) * 4);

	yellow1->Start(time(NULL) * 5);

	purple1->Start(time(NULL) * 6);
	purple2->Start(time(NULL) * 7);

	green1->Start(time(NULL) * 8);
	green2->Start(time(NULL) * 9);

	blue1->Start(time(NULL) * 10);
	blue2->Start(time(NULL) * 11);

	return true;
}

void Animation::Update(float a_deltaTime)
{
	// update our camera matrix using the keyboard/mouse
	Utility::freeMovement(m_cameraMatrix, a_deltaTime, 10);

	// clear all gizmos from last frame
	Gizmos::clear();

	// Update arena so it can continue to render
	stage.Update(a_deltaTime);

	// Update all actors with time since last frame
	red1->Update(a_deltaTime);
	red2->Update(a_deltaTime);
	red3->Update(a_deltaTime);
	red4->Update(a_deltaTime);

	yellow1->Update(a_deltaTime);

	purple1->Update(a_deltaTime);
	purple2->Update(a_deltaTime);

	green1->Update(a_deltaTime);
	green2->Update(a_deltaTime);

	blue1->Update(a_deltaTime);
	blue2->Update(a_deltaTime);

	// For each actor, check all other actors to see if they are in range of it
	red1->CheckInRange(red2);
	red1->CheckInRange(red3);
	red1->CheckInRange(red4);
	red1->CheckInRange(yellow1);
	red1->CheckInRange(purple1);
	red1->CheckInRange(purple2);
	red1->CheckInRange(green1);
	red1->CheckInRange(green2);
	red1->CheckInRange(blue1);
	red1->CheckInRange(blue2);

	red2->CheckInRange(red1);
	red2->CheckInRange(red3);
	red2->CheckInRange(red4);
	red2->CheckInRange(yellow1);
	red2->CheckInRange(purple1);
	red2->CheckInRange(purple2);
	red2->CheckInRange(green1);
	red2->CheckInRange(green2);
	red2->CheckInRange(blue1);
	red2->CheckInRange(blue2);

	red3->CheckInRange(red1);
	red3->CheckInRange(red2);
	red3->CheckInRange(red4);
	red3->CheckInRange(yellow1);
	red3->CheckInRange(purple1);
	red3->CheckInRange(purple2);
	red3->CheckInRange(green1);
	red3->CheckInRange(green2);
	red3->CheckInRange(blue1);
	red3->CheckInRange(blue2);

	red4->CheckInRange(red1);
	red4->CheckInRange(red2);
	red4->CheckInRange(red3);
	red4->CheckInRange(yellow1);
	red4->CheckInRange(purple1);
	red4->CheckInRange(purple2);
	red4->CheckInRange(green1);
	red4->CheckInRange(green2);
	red4->CheckInRange(blue1);
	red4->CheckInRange(blue2);

	yellow1->CheckInRange(red1);
	yellow1->CheckInRange(red2);
	yellow1->CheckInRange(red3);
	yellow1->CheckInRange(red4);
	yellow1->CheckInRange(purple1);
	yellow1->CheckInRange(purple2);
	yellow1->CheckInRange(green1);
	yellow1->CheckInRange(green2);
	yellow1->CheckInRange(blue1);
	yellow1->CheckInRange(blue1);

	purple1->CheckInRange(red1);
	purple1->CheckInRange(red2);
	purple1->CheckInRange(red3);
	purple1->CheckInRange(red4);
	purple1->CheckInRange(yellow1);
	purple1->CheckInRange(purple2);
	purple1->CheckInRange(green1);
	purple1->CheckInRange(green2);
	purple1->CheckInRange(blue1);
	purple1->CheckInRange(blue2);

	purple2->CheckInRange(red1);
	purple2->CheckInRange(red2);
	purple2->CheckInRange(red3);
	purple2->CheckInRange(red4);
	purple2->CheckInRange(yellow1);
	purple2->CheckInRange(purple1);
	purple2->CheckInRange(green1);
	purple2->CheckInRange(green2);
	purple2->CheckInRange(blue1);
	purple2->CheckInRange(blue2);

	green1->CheckInRange(red1);
	green1->CheckInRange(red2);
	green1->CheckInRange(red3);
	green1->CheckInRange(red4);
	green1->CheckInRange(yellow1);
	green1->CheckInRange(purple1);
	green1->CheckInRange(purple2);
	green1->CheckInRange(green2);
	green1->CheckInRange(blue1);
	green1->CheckInRange(blue2);

	green2->CheckInRange(red1);
	green2->CheckInRange(red2);
	green2->CheckInRange(red3);
	green2->CheckInRange(red4);
	green2->CheckInRange(yellow1);
	green2->CheckInRange(purple1);
	green2->CheckInRange(purple2);
	green2->CheckInRange(green1);
	green2->CheckInRange(blue1);
	green2->CheckInRange(blue2);

	blue1->CheckInRange(red1);
	blue1->CheckInRange(red2);
	blue1->CheckInRange(red3);
	blue1->CheckInRange(red4);
	blue1->CheckInRange(yellow1);
	blue1->CheckInRange(purple1);
	blue1->CheckInRange(purple2);
	blue1->CheckInRange(green1);
	blue1->CheckInRange(green2);
	blue1->CheckInRange(blue2);

	blue2->CheckInRange(red1);
	blue2->CheckInRange(red2);
	blue2->CheckInRange(red3);
	blue2->CheckInRange(red4);
	blue2->CheckInRange(yellow1);
	blue2->CheckInRange(purple1);
	blue2->CheckInRange(purple2);
	blue2->CheckInRange(green1);
	blue2->CheckInRange(green2);
	blue2->CheckInRange(blue1);

	// add an identity matrix gizmo
	Gizmos::addTransform(glm::mat4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1));
	// add a 20x20 grid on the XZ-plane
	for (int i = 0; i < 21; ++i)
	{
		Gizmos::addLine(glm::vec3(-10 + i, 0, 10), glm::vec3(-10 + i, 0, -10),
			i == 10 ? glm::vec4(1, 1, 1, 1) : glm::vec4(0, 0, 0, 1));

		Gizmos::addLine(glm::vec3(10, 0, -10 + i), glm::vec3(-10, 0, -10 + i),
			i == 10 ? glm::vec4(1, 1, 1, 1) : glm::vec4(0, 0, 0, 1));
	}

	// grab the skeleton and animation we want to use
	/*FBXSkeleton* skeleton = m_fbxModel->getSkeletonByIndex(0);
	FBXAnimation* animation = m_fbxModel->getAnimationByIndex(0);

	FBXSkeleton* skeleton2 = m_fbxModel2->getSkeletonByIndex(0);
	FBXAnimation* animation2 = m_fbxModel2->getAnimationByIndex(0);*/

	/*cout << m_fbxModel2->getSkeletonCount() << endl;*/


	// evaluate the animation to update bones
	/*skeleton->evaluate(animation, Utility::getTotalTime());
	skeleton2->evaluate(animation2, Utility::getTotalTime());*/
	glm::mat4 m4Model;
	glm::mat4 m4Model2;
	//Update our FBX model
	//for (unsigned int i = 0; i < m_fbxModel->getMeshCount(); ++i)
	//{
	//	// get the current mesh
	//	FBXMeshNode *mesh = m_fbxModel->getMeshByIndex(i);
	//	// if you move an object around within your scene
	//	// children nodes are not updated until this function is called.
	//	mesh->updateGlobalTransform();
	//	m4Model = m_modelMatrix * mesh->m_globalTransform;
	//	
	//}

	//for (unsigned int i = 0; i < m_fbxModel2->getMeshCount(); ++i)
	//{
	//	// get the current mesh
	//	FBXMeshNode *mesh2 = m_fbxModel2->getMeshByIndex(i);
	//	// if you move an object around within your scene
	//	// children nodes are not updated until this function is called.
	//	mesh2->updateGlobalTransform();
	//	m4Model2 = m_modelMatrix2 * mesh2->m_globalTransform;

	//}
	//if (m_modelMatrix[3].z - m_modelMatrix2[3].z >= 0.5f || m_modelMatrix[3].z - m_modelMatrix2[3].z <= -0.5f)
	//{
	//	if (moveUp)
	//	{
	//		m_modelMatrix[3].z -= 0.1f;
	//		m_modelMatrix2[3].z += 0.1f;
	//		if (m_modelMatrix[3].z >= 5.f)
	//		{
	//			moveUp = false;
	//		}
	//	}
	//	else
	//	{
	//		m_modelMatrix[3].z += 0.1f;
	//		m_modelMatrix2[3].z -= 0.1f;
	//		if (m_modelMatrix[3].z <= -5.f)
	//		{
	//			moveUp = true;
	//		}
	//	}
	//}

	//// update the bones to include the bind pose
	//// so that the offset is local
	//skeleton->updateBones();
	//skeleton2->updateBones();
	//This is a small routine to draw the bones and their positions for the model.
	//for (int i = 0; i < skeleton->m_boneCount; ++i)
	//{
	//	glm::mat4 boneM4 = m4Model * (skeleton->m_bones[i] * glm::inverse(skeleton->m_bindPoses[i]));// ->m_localTransform;
	//	Gizmos::addSphere(boneM4[3], 8, 8, 0.005, glm::vec4(1.f, 0.f, 0.5f, 1.f));
	//	if (skeleton->m_parentIndex[i] != -1)
	//	{
	//		glm::mat4 parentM4 = m4Model * (skeleton->m_bones[skeleton->m_parentIndex[i]] * glm::inverse(skeleton->m_bindPoses[skeleton->m_parentIndex[i]]));
	//		Gizmos::addLine(parentM4[3], boneM4[3], glm::vec4(1, 1, 1, 1));
	//	}
	//}

	//for (int i = 0; i < skeleton2->m_boneCount; ++i)
	//{
	//	glm::mat4 boneM42 = m4Model2 * (skeleton2->m_bones[i] * glm::inverse(skeleton2->m_bindPoses[i]));// ->m_localTransform;
	//	Gizmos::addSphere(boneM42[3], 8, 8, 0.005, glm::vec4(1.f, 0.f, 0.5f, 1.f));
	//	if (skeleton2->m_parentIndex[i] != -1)
	//	{
	//		glm::mat4 parentM42 = m4Model2 * (skeleton2->m_bones[skeleton2->m_parentIndex[i]] * glm::inverse(skeleton2->m_bindPoses[skeleton2->m_parentIndex[i]]));
	//		Gizmos::addLine(parentM42[3], boneM42[3], glm::vec4(1, 1, 1, 1));
	//	}
	//}

	// quit our application when escape is pressed
	if (glfwGetKey(m_window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		quit();
}

void Animation::Draw()
{
	// clear the backbuffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// get the view matrix from the world-space camera matrix
	glm::mat4 viewMatrix = glm::inverse(m_cameraMatrix);

	//bing our shader program
	glUseProgram(m_programID);
	//bind our vertex array object
	glBindVertexArray(m_vao);

	//get our shaders uniform location for our projectionViewMatrix and then use glUniformMatrix4fv to fill it with the correct data
	unsigned int projectionViewUniform = glGetUniformLocation(m_programID, "ProjectionView");
	glUniformMatrix4fv(projectionViewUniform, 1, false, glm::value_ptr(m_projectionMatrix * viewMatrix));

	//pass our camera position to our fragment shader uniform
	unsigned int cameraPosUniform = glGetUniformLocation(m_programID, "cameraPosition");
	glUniform4fv(cameraPosUniform, 1, glm::value_ptr(m_cameraMatrix[3]));


	//pass the directional light direction to our fragment shader
	glm::vec4 lightDir = glm::vec4(-0.40f, -0.20f, 0.60f, 0.f);
	unsigned int lightDirUniform = glGetUniformLocation(m_programID, "lightDirection");
	glUniform4fv(lightDirUniform, 1, glm::value_ptr(lightDir));



	// bind the array of bones
	/*FBXSkeleton* skeleton = m_fbxModel->getSkeletonByIndex(0);
	int boneLocation = glGetUniformLocation(m_programID, "Bones");
	glUniformMatrix4fv(boneLocation, skeleton->m_boneCount, GL_FALSE, (float*)skeleton->m_bones);

	FBXSkeleton* skeleton2 = m_fbxModel2->getSkeletonByIndex(0);
	int boneLocation2 = glGetUniformLocation(m_programID, "Bones");
	glUniformMatrix4fv(boneLocation2, skeleton2->m_boneCount, GL_FALSE, (float*)skeleton->m_bones);*/

	//Draw our FBX Model

	// for each mesh in the model...
	//for (unsigned int i = 0; i < m_fbxModel->getMeshCount(); ++i)
	//{
	//	// get the current mesh
	//	FBXMeshNode *pMesh = m_fbxModel->getMeshByIndex(i);

	//	// send the Model
	//	glm::mat4 m4Model = m_modelMatrix * pMesh->m_globalTransform;
	//	unsigned int modelUniform = glGetUniformLocation(m_programID, "Model");
	//	glUniformMatrix4fv(modelUniform, 1, false, glm::value_ptr(m4Model));


	//	// Bind the texture to one of the ActiveTextures
	//	// if your shader supported multiple textures, you would bind each texture to a new Active Texture ID here
	//	//bind our textureLocation variable from the shaders and set it's value to 0 as the active texture is texture 0
	//	unsigned int texUniformID = glGetUniformLocation(m_programID, "DiffuseTexture");
	//	glUniform1i(texUniformID, 0);
	//	//set our active texture, and bind our loaded texture
	//	glActiveTexture(GL_TEXTURE0);
	//	glBindTexture(GL_TEXTURE_2D, pMesh->m_material->textureIDs[FBXMaterial::DiffuseTexture]);

	//	// Send the vertex data to the VBO
	//	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	//	glBufferData(GL_ARRAY_BUFFER, pMesh->m_vertices.size() * sizeof(FBXVertex), pMesh->m_vertices.data(), GL_STATIC_DRAW);

	//	// send the index data to the IBO
	//	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
	//	glBufferData(GL_ELEMENT_ARRAY_BUFFER, pMesh->m_indices.size() * sizeof(unsigned int), pMesh->m_indices.data(), GL_STATIC_DRAW);

	//	glDrawElements(GL_TRIANGLES, pMesh->m_indices.size(), GL_UNSIGNED_INT, 0);
	//}

	//for (unsigned int i = 0; i < m_fbxModel2->getMeshCount(); ++i)
	//{
	//	// get the current mesh
	//	FBXMeshNode *pMesh2 = m_fbxModel2->getMeshByIndex(i);

	//	// send the Model
	//	glm::mat4 m4Model2 = m_modelMatrix2 * pMesh2->m_globalTransform;
	//	unsigned int modelUniform = glGetUniformLocation(m_programID, "Model");
	//	glUniformMatrix4fv(modelUniform, 1, false, glm::value_ptr(m4Model2));


	//	// Bind the texture to one of the ActiveTextures
	//	// if your shader supported multiple textures, you would bind each texture to a new Active Texture ID here
	//	//bind our textureLocation variable from the shaders and set it's value to 0 as the active texture is texture 0
	//	unsigned int texUniformID2 = glGetUniformLocation(m_programID, "DiffuseTexture");
	//	glUniform1i(texUniformID2, 0);
	//	//set our active texture, and bind our loaded texture
	//	glActiveTexture(GL_TEXTURE0);
	//	glBindTexture(GL_TEXTURE_2D, pMesh2->m_material->textureIDs[FBXMaterial::DiffuseTexture]);

	//	// Send the vertex data to the VBO
	//	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	//	glBufferData(GL_ARRAY_BUFFER, pMesh2->m_vertices.size() * sizeof(FBXVertex), pMesh2->m_vertices.data(), GL_STATIC_DRAW);

	//	// send the index data to the IBO
	//	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
	//	glBufferData(GL_ELEMENT_ARRAY_BUFFER, pMesh2->m_indices.size() * sizeof(unsigned int), pMesh2->m_indices.data(), GL_STATIC_DRAW);

	//	glDrawElements(GL_TRIANGLES, pMesh2->m_indices.size(), GL_UNSIGNED_INT, 0);
	//}

	glBindVertexArray(0);
	glUseProgram(0);

	// draw the gizmos from this frame
	Gizmos::draw(viewMatrix, m_projectionMatrix);

}

void Animation::Destroy()
{

	/*m_fbxModel->unload();
	delete m_fbxModel;

	m_fbxModel2->unload();
	delete m_fbxModel2;*/

	glDeleteBuffers(1, &m_vbo);
	glDeleteBuffers(1, &m_ibo);
	glDeleteVertexArrays(1, &m_vao);
	glDeleteProgram(m_programID);
	glDeleteShader(m_fragmentShader);
	glDeleteShader(m_vertexShader);

	Gizmos::destroy();

	// Delete all actors
	delete red1;
	delete red2;
	delete red3;
	delete red4;
	delete yellow1;
	delete purple1;
	delete purple2;
	delete green1;
	delete green2;
	delete blue1;
	delete blue2;

}