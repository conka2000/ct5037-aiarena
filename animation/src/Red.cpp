#include "Red.h"

using namespace std;

Red::Red(char const* a_name)
{
	// Set class to red with red colour and set stats
	SetUp(a_name, 'R', glm::vec4(1,0,0,1), 20.f, 3.f, 70.f, 1.2f);
}

Red::~Red()
{
	
}

// If unit is in vision range
void Red::CheckInRange(Actor* otherActor)
{
	// If unit is not dead find other units in vision range that aren't dead
	if (!GetDead())
	{
		if (!otherActor->GetDead())
		{
			if (A_CheckInRange(otherActor))
			{
				InRange(otherActor); // Unit in range found
			}
			else if (otherActor == GetInteractedActor())
			{
				SetBehaviour(0); // Go back to wandering
			}
		}
		else if (otherActor == GetInteractedActor())
		{
			SetBehaviour(0); // Go back to wandering
		}
	}
}

void Red::InRange(Actor* otherActor)
{
	if (otherActor == GetInteractedActor()) // If the actor is the one this unit currently interacting with
	{
		return;
	}
	else if (GetInteractedActor() == NULL && GetBehaviour() == 0) // If this unit is not interacting with another and is wandering
	{
		if (!otherActor->GetDead())
		{
			SetInteractedActor(otherActor); // Set the actor we are interacting with
			switch (GetBehaviour())
			{
			case 0: //WANDER
				SetBehaviour(2); // Fight any other unit
				break;
			case 1: //FLEE


				break;
			case 2: //CHASE


				break;
			case 3: //FIGHT


				break;
			}
		}
	}
}