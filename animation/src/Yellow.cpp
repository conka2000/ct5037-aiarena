#include "Yellow.h"

using namespace std;

Yellow::Yellow(char const* a_name)
{
	// Set class to yellow with yellow colour and set stats
	SetUp(a_name, 'Y' , glm::vec4(1, 1, 0, 1), 20.f, 1.f, 70.f, 1.f);
}

Yellow::~Yellow()
{

}

void Yellow::CheckInRange(Actor* otherActor)
{
	// If unit is not dead find other units in vision range that aren't dead
	if (!GetDead())
	{
		if (!otherActor->GetDead())
		{
			if (A_CheckInRange(otherActor))
			{
				InRange(otherActor); // Unit in range found
			}
			else if (otherActor == GetInteractedActor())
			{
				SetBehaviour(0); // Go back to wandering
			}
		}
		else if (otherActor == GetInteractedActor())
		{
			SetBehaviour(0); // Go back to wandering
		}
	}
}

// If unit is in vision range
void Yellow::InRange(Actor* otherActor)
{
	if (otherActor == GetInteractedActor()) // If the actor is the one this unit currently interacting with
	{
		return;
	}
	else if (GetInteractedActor() == NULL && GetBehaviour() == 0) // If this unit is not interacting with another and is wandering
	{
		if (!otherActor->GetDead())
		{
			SetInteractedActor(otherActor); // Set the actor we are interacting with
			switch (GetBehaviour())
			{
			case 0: //WANDER
				if (otherActor->GetClass() != 'B') // Flee from all other units except for Blue units
				{
					SetBehaviour(1);
				}
				break;
			case 1: //FLEE


				break;
			case 2: //CHASE


				break;
			case 3: //FIGHT

				break;
			}
		}
	}
}