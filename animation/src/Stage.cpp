#include "Stage.h"

Stage::Stage()
{

}

Stage::~Stage()
{

}

// Renders all walls and turn nodes
void Stage::Update(float a_deltaTime)
{
	// Procedurally render each wall in arena specifically
	
	// This for loop is for outside boundaries
	for (float i = -21 * square; i <= 21 * square; i++)
	{
		Gizmos::addBox(glm::vec3(i, square, -21 * square), glm::vec3(square, square, square), true, wallColour);
		Gizmos::addBox(glm::vec3(i, square, 21 * square), glm::vec3(square, square, square), true, wallColour);

		Gizmos::addBox(glm::vec3(-21 * square, square, i), glm::vec3(square, square, square), true, wallColour);
		Gizmos::addBox(glm::vec3(21 * square, square, i), glm::vec3(square, square, square), true, wallColour);
	}

	// Each for loop is a line across the arena with walls (including gaps) (horizontal or vertical)
	for (float i = -21 * square; i <= 21 * square; i++)
	{
		if ((i >= -19 * square && i <= -13 * square) || (i >= -5 * square && i <= 9 * square))
		{
			Gizmos::addBox(glm::vec3(i, square, 5 * square), glm::vec3(square, square, square), true, wallColour);
		}

		if ((i >= 7 * square && i <= 16 * square))
		{
			Gizmos::addBox(glm::vec3(i, square, -1 * square), glm::vec3(square, square, square), true, wallColour);
		}

		if ((i >= -7 * square && i <= -2 * square))
		{
			Gizmos::addBox(glm::vec3(i, square, -11 * square), glm::vec3(square, square, square), true, wallColour);
		}

		if ((i >= 0 * square && i <= 5 * square) || (i >= -15 * square && i <= -2 * square))
		{
			Gizmos::addBox(glm::vec3(-5 * square, square, i), glm::vec3(square, square, square), true, wallColour);
		}

		if ((i >= 15 * square && i <= 19 * square) || (i >= -15 * square && i <= -5 * square))
		{
			Gizmos::addBox(glm::vec3(9 * square, square, i), glm::vec3(square, square, square), true, wallColour);
		}
	}

	// Add all the turn nodes to the arena
	Gizmos::addSphere(glm::vec3(-11 * square, -square, 3 * square), 9, 9, square, nodeColour);
	Gizmos::addSphere(glm::vec3(-11 * square, -square, 7 * square), 9, 9, square, nodeColour);

	Gizmos::addSphere(glm::vec3(11 * square, -square, 13 * square), 9, 9, square, nodeColour);
	Gizmos::addSphere(glm::vec3(7 * square, -square, 13 * square), 9, 9, square, nodeColour);

	Gizmos::addSphere(glm::vec3(11 * square, -square, -17 * square), 9, 9, square, nodeColour);
	Gizmos::addSphere(glm::vec3(7 * square, -square, -17 * square), 9, 9, square, nodeColour);

	Gizmos::addSphere(glm::vec3(11 * square, -square, -3 * square), 9, 9, square, nodeColour);
	Gizmos::addSphere(glm::vec3(7 * square, -square, -3 * square), 9, 9, square, nodeColour);

	Gizmos::addSphere(glm::vec3(-7 * square, -square, -1 * square), 9, 9, square, nodeColour);
	Gizmos::addSphere(glm::vec3(-3 * square, -square, -1 * square), 9, 9, square, nodeColour);

	Gizmos::addSphere(glm::vec3(-7 * square, -square, -17 * square), 9, 9, square, nodeColour);
	Gizmos::addSphere(glm::vec3(-3 * square, -square, -17 * square), 9, 9, square, nodeColour);

	Gizmos::addSphere(glm::vec3(-9 * square, -square, -9 * square), 9, 9, square, nodeColour);
	Gizmos::addSphere(glm::vec3(-1 * square, -square, -9 * square), 9, 9, square, nodeColour);

	Gizmos::addSphere(glm::vec3(-7 * square, -square, 7 * square), 9, 9, square, nodeColour);

	Gizmos::addSphere(glm::vec3(-1 * square, -square, -13 * square), 9, 9, square, nodeColour);
	Gizmos::addSphere(glm::vec3(-9 * square, -square, -13 * square), 9, 9, square, nodeColour);

	Gizmos::addSphere(glm::vec3(-7 * square, -square, -1 * square), 9, 9, square, nodeColour);
	Gizmos::addSphere(glm::vec3(-3 * square, -square, -1 * square), 9, 9, square, nodeColour);

	Gizmos::addSphere(glm::vec3(5 * square, -square, 1 * square), 9, 9, square, nodeColour);
	Gizmos::addSphere(glm::vec3(5 * square, -square, -3 * square), 9, 9, square, nodeColour);

	Gizmos::addSphere(glm::vec3(17 * square, -square, 1 * square), 9, 9, square, nodeColour);
	Gizmos::addSphere(glm::vec3(17 * square, -square, -3 * square), 9, 9, square, nodeColour);

	Gizmos::addSphere(glm::vec3(11 * square, -square, 3 * square), 9, 9, square, nodeColour);
	Gizmos::addSphere(glm::vec3(11 * square, -square, 7 * square), 9, 9, square, nodeColour);
}

void Stage::Draw(const glm::mat4 viewMatrix ,const glm::mat4& a_projectionView)
{
	
}
