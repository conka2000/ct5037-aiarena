#include "Green.h"

using namespace std;

Green::Green(char const* a_name)
{
	// Set class to green with green colour and set stats
	SetUp(a_name, 'G', glm::vec4(0, 1, 0, 1), 25.f, 2.f, 50.f, 1.4f);
}

Green::~Green()
{

}

void Green::CheckInRange(Actor* otherActor)
{
	// If unit is not dead find other units in vision range that aren't dead
	if (!GetDead())
	{
		if (!otherActor->GetDead())
		{
			if (A_CheckInRange(otherActor))
			{
				InRange(otherActor); // Unit in range found
			}
			else if (otherActor == GetInteractedActor())
			{
				SetBehaviour(0); // Go back to wandering
			}
		}
		else if (otherActor == GetInteractedActor())
		{
			SetBehaviour(0); // Go back to wandering
		}
	}
}

// If unit is in vision range
void Green::InRange(Actor* otherActor)
{
	if (otherActor == GetInteractedActor()) // If the actor is the one this unit currently interacting with
	{
		return;
	}
	else if (GetInteractedActor() == NULL && GetBehaviour() == 0) // If this unit is not interacting with another and is wandering
	{
		if (!otherActor->GetDead())
		{
			SetInteractedActor(otherActor); // Set the actor we are interacting with
			switch (GetBehaviour())
			{
			case 0: //WANDER
				if (otherActor->GetClass() == 'R') // Flee from Red units
				{
					SetBehaviour(1);
				}
				else if (otherActor->GetClass() == 'P') // Chase Purple units
				{
					SetBehaviour(2);
				}
				break;
			case 1: //FLEE


				break;
			case 2: //CHASE


				break;
			case 3: //FIGHT


				break;
			}
		}
	}
}