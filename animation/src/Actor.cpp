#include "Actor.h"

using namespace std;

// s1504719 - Cameron Seeley

Actor::Actor()
{

}

Actor::~Actor()
{

}

// Set all variables exclusive to class
void Actor::SetUp(char const* a_name, char cType, glm::vec4 color, float vision, float attack, float a_health, float a_speed)
{
	// Set name, class, colour, vision range, attack power, max health and speed

	name = a_name;
	cClass = cType;
	colour = color;
	visionRange = vision;
	attackPower = attack;
	maxHealth = a_health;
	health = maxHealth;
	speed = a_speed;
	dead = false;
}

// Initialise the actor
void Actor::Start(float a_seed)
{
	// Set seed
	seed = a_seed;
	srand(seed);

	// Set wall boundaries

	walls[0] = glm::vec4(-20, 4, -12, 6);
	walls[1] = glm::vec4(-6, 4, 10, 6);
	walls[2] = glm::vec4(-8, -12, -2, -10);
	walls[3] = glm::vec4(6, -2, 16, 0);
	walls[4] = glm::vec4(8, 14, 10, 20);
	walls[5] = glm::vec4(-6, 0, -4, 6);
	walls[6] = glm::vec4(-6, -16, -4, -2);
	walls[7] = glm::vec4(8, -16, 10, -4);

	// Set turn nodes

	tNodes[0] = glm::vec2(-13, 7);
	tNodes[1] = glm::vec2(-13, 5);
	tNodes[2] = glm::vec2(-7, 7);
	tNodes[3] = glm::vec2(-11, 7);
	tNodes[4] = glm::vec2(-11, -3);
	tNodes[5] = glm::vec2(-9, -13);
	tNodes[6] = glm::vec2(-9, -11);
	tNodes[7] = glm::vec2(-1, -13);
	tNodes[8] = glm::vec2(-1, -11);
	tNodes[9] = glm::vec2(5, -1);
	tNodes[10] = glm::vec2(5, -3);
	tNodes[11] = glm::vec2(17, -1);
	tNodes[12] = glm::vec2(17, -3);
	tNodes[13] = glm::vec2(7, 13);
	tNodes[14] = glm::vec2(11 ,13);
	tNodes[15] = glm::vec2(-7, 1);
	tNodes[16] = glm::vec2(-3, 1);
	tNodes[17] = glm::vec2(-7, -17);
	tNodes[18] = glm::vec2(-3, -17);
	tNodes[19] = glm::vec2(7, -17);
	tNodes[20] = glm::vec2(11, -17);
	tNodes[21] = glm::vec2(7, -3);
	tNodes[22] = glm::vec2(11, -3);

	// Set a random intial position
	do
	{
		cout << "Spawn" << endl;
		if (rand() % 2 <= 0)
		{
			alignment = -1;
		}
		else
		{
			alignment = 1;
		}

		position.x = alignment * (rand() % 19);

		if (rand() % 2 >= 1)
		{
			alignment = 1;
		}
		else
		{
			alignment = -1;
		}

		position.z = alignment * (rand() % 19);
	} while (CheckInWall(position)); // Don't spawn in walls
	

	// Random initial wander time

	allowedWanderTime = rand() % 3 + 2;

	// Random initial direction

	NewVelocity();

}

// Done every frame
void Actor::Update(float a_deltaTime)
{
	if (!dead) // If this actor is not dead
	{
		// Face current direction of movement
		RotateTo(velocity);

		switch (behaviour)
		{
		case WANDER: // No other interactions so just walk around
			wanderTimer += a_deltaTime; // Count time

			if (wanderTimer >= allowedWanderTime) //After certain time, change direction, timer and speed randomly
			{
				NewVelocity();

				wanderTimer = 0.f;

				cout << name << "'s position: (" << position.x << ", " << position.z << ")" << endl;

				allowedWanderTime = rand() % 3 + 2;
			}

			position += velocity * a_deltaTime; // Move unit

			break;
		case FLEE: // If actor is running away from another actor
			if (!interactedActor->GetDead())
			{
				if (sqrt(powf(position.x - interactedActor->GetPos().x, 2) + powf(position.z - interactedActor->GetPos().z, 2)) <= visionRange) 
					// If distance between this actor and other actor is less than the vision range of this actor
				{
					if (sqrt(powf(position.x - interactedActor->GetPos().x, 2) + powf(position.z - interactedActor->GetPos().z, 2)) <= 1.1)
						// If distance between this actor and other actor is very close then fight
					{
						SetBehaviour(3);
					}
					else
					{
						if (nodeSet) // If running to turn node
						{
							if (sqrt(powf(position.x - currentNode.x, 2) + powf(position.z - currentNode.y, 2)) <= 0.5f || CheckInWall(position))
								// If arrived at the turn node or hit a wall, then reset flee
							{
								nodeSet = false;
								lastNode = currentNode;
							}
							else
							{
								vecBetween = glm::vec3(currentNode.x, 0, currentNode.y) - position; // Way to turn node

								float divAmount = sqrtf(powf(vecBetween.x, 2) + powf(vecBetween.z, 2));

								// Move to turn node
								velocity.x = (vecBetween.x / divAmount) * 5;
								velocity.z = (vecBetween.z / divAmount) * 5;
							}
						}
						else // Choose turn node to go to
						{
							// Node placeholders (Third of vec3 is distance)
							glm::vec3 nodes[4] = { glm::vec3(0, 0, 100), glm::vec3(0, 0, 100), glm::vec3(0, 0, 100), glm::vec3(0, 0, 100) };

							// Distance placeholder
							float dist; 
							
							for (int i = 0; i <= ((sizeof tNodes) / (sizeof *tNodes)); i++) // For all turn nodes on map
							{
								dist = sqrt(powf(position.x - tNodes[i].x, 2) + powf(position.z - tNodes[i].y, 2));
								// If distance between this actor and the turn node is less than current chosen nodes (Impossible to exceed 100)
								// Replace chosen node, keep 4 closest nodes stored
								if (dist <= nodes[0].z)
								{
									nodes[0].xy = tNodes[i];
									nodes[0].z = dist;
								}
								else if (dist <= nodes[1].z)
								{
									nodes[1].xy = tNodes[i];
									nodes[1].z = dist;
								}
								else if (dist <= nodes[2].z)
								{
									nodes[2].xy = tNodes[i];
									nodes[2].z = dist;
								}
								else if (dist <= nodes[3].z)
								{
									nodes[3].xy = tNodes[i];
									nodes[3].z = dist;
								}
							}

							// Position of actor this actor is running from
							glm::vec3 e_Pos = GetInteractedActor()->GetPos();
							float e_Dist;
							// Node placeholder (Third of vec3 is distance)
							glm::vec3 theNode = glm::vec3(0, 0, 0);

							// For the 4 nodes chosen earlier
							for (int j = 1; j < ((sizeof nodes) / (sizeof *nodes)); j++)
							{
								e_Dist = sqrt(powf(e_Pos.x - nodes[j].x, 2) + powf(e_Pos.z - nodes[j].y, 2));
								// Distance between actor running from and turn node
								if (e_Dist >= theNode.z && !CheckWallsBetween(nodes[j]) && glm::vec2(nodes[j].x, nodes[j].y) != glm::vec2(currentNode.x, currentNode.y))
								{
									// Choose turn node that has highest distance from actor running from, also not behind wall and not one recently used
									theNode = nodes[j];
									currentNode = nodes[j].xy;
									nodeSet = true;
								}
							}
						}

					}
				}
				else // If out of range go back to wandering
				{
					SetBehaviour(0);
				}
			}
			else // If other actor dies go back to wandering
			{
				SetBehaviour(0);
			}

			position += velocity * a_deltaTime * speed; // Move unit

			break;
		case CHASE: // When actor is chasing another to fight it
			if (!interactedActor->GetDead())
			{
				if (sqrt(powf(position.x - interactedActor->GetPos().x, 2) + powf(position.z - interactedActor->GetPos().z, 2)) >= 2.f)
					// If distance between this actor and other actor is not too close
				{
					if (nodeSet) // If moving to node
					{
						if (sqrt(powf(position.x - currentNode.x, 2) + powf(position.z - currentNode.y, 2)) <= 0.5f) 
							// If at node then reset chase
						{
							nodeSet = false;
							lastNode = currentNode;
						}
						else
						{
							vecBetween = glm::vec3(currentNode.x, 0, currentNode.y) - position; // Direction to turn node

							float divAmount = sqrtf(powf(vecBetween.x, 2) + powf(vecBetween.z, 2)); 

							// Move to turn node
							velocity.x = (vecBetween.x / divAmount) * 5;
							velocity.z = (vecBetween.z / divAmount) * 5;
						}
					}
					else // If just chasing other actor
					{
						vecBetween = interactedActor->GetPos() - position; // Direction to other actor

						float divAmount = sqrtf(powf(vecBetween.x, 2) + powf(vecBetween.z, 2));

						// Move to other actor
						velocity.x = (vecBetween.x / divAmount) * 3;
						velocity.z = (vecBetween.z / divAmount) * 3;
					}
				}
				else // If very close then start fighting other actor
				{
					SetBehaviour(3);
				}
			}
			else // If other actor dies then go back to wandering
			{
				SetBehaviour(0);
			}

			position += velocity * a_deltaTime * speed; // Move unit

			break;
		case FIGHT: // When unit is fighting with other unit trying to kill it
			if (!interactedActor->GetDead()) 
			{
				if (sqrt(powf(position.x - interactedActor->GetPos().x, 2) + powf(position.z - interactedActor->GetPos().z, 2)) <= 3.f)
					// If distance between this actor and other actor is very close
				{
					// Force other actor to fight also
					interactedActor->SetInteractedActor(this);
					interactedActor->SetBehaviour(3);

					// Hurt the other actor
					interactedActor->SetHealth(interactedActor->GetHealth() - (attackPower * a_deltaTime * 8)); 
					// Face the other actor
					RotateTo(interactedActor->GetPos());
				}
				else // If not close then go back to chasing other actor
				{
					SetBehaviour(2);
				}
			}
			else // If other actor dies go back to wandering
			{
				SetBehaviour(0);
			}
			break;
		}

		if (CheckInWall(position)) // If actor is in a wall
		{
			if (GetBehaviour() == 1 && nodeSet) // If moving to a turn node and fleeing
			{
				// Reset flee
				nodeSet = false;
				lastNode = currentNode;

				position -= velocity * a_deltaTime * speed;
				NewVelocity();
			}
			else if (GetBehaviour() == 2) // If chasing another actor
			{
				// Choose a node to move to (similar to chosing node for fleeing but want a node close to the other actor)
				glm::vec3 nodes[4] = { glm::vec3(0, 0, 100), glm::vec3(0, 0, 100), glm::vec3(0, 0, 100), glm::vec3(0, 0, 100) };

				float dist;
				for (int i = 0; i <= ((sizeof tNodes) / (sizeof *tNodes)); i++)
				{
					dist = sqrt(powf(position.x - tNodes[i].x, 2) + powf(position.z - tNodes[i].y, 2));
					if (dist <= nodes[0].z)
					{
						nodes[0].xy = tNodes[i];
						nodes[0].z = dist;
					}
					else if (dist <= nodes[1].z)
					{
						nodes[1].xy = tNodes[i];
						nodes[1].z = dist;
					}
					else if (dist <= nodes[2].z)
					{
						nodes[2].xy = tNodes[i];
						nodes[2].z = dist;
					}
					else if (dist <= nodes[3].z)
					{
						nodes[3].xy = tNodes[i];
						nodes[3].z = dist;
					}
				}

				glm::vec3 e_Pos = GetInteractedActor()->GetPos();
				float e_Dist;
				glm::vec3 theNode = glm::vec3(0, 0, 100);

				for (int j = 1; j < ((sizeof nodes) / (sizeof *nodes)); j++)
				{
					e_Dist = sqrt(powf(e_Pos.x - nodes[j].x, 2) + powf(e_Pos.z - nodes[j].y, 2));
					if (e_Dist <= theNode.z && glm::vec2(nodes[j].x, nodes[j].y) != glm::vec2(currentNode.x, currentNode.y) && glm::vec2(nodes[j].x, nodes[j].y) != glm::vec2(lastNode.x, lastNode.y))
					{ // Want node closet to the actor we are chasing
						theNode = nodes[j];
						currentNode = nodes[j].xy;
						nodeSet = true;
					}
				}
			}
			else // If not fleeing with turn node or chasing change direction
			{
				position -= velocity * a_deltaTime * speed;
				NewVelocity();
			}
		}

		// If actor is hitting an outside boundary, change direction
		if (position.x + 0.1 >= xbound)
		{
			position.x = xbound - 0.1;
			NewVelocity();
		}
		if (position.x - 0.1 <= -xbound)
		{
			position.x = -xbound + 0.1;
			NewVelocity();
		}
		if (position.z + 0.1 >= zbound)
		{
			position.z = zbound - 0.1;
			NewVelocity();
		}
		if (position.z - 0.1 <= -zbound)
		{
			position.z = -zbound + 0.1;
			NewVelocity();
		}

		// The actor's colour will change closer to white the more hurt it is

		if (cClass == 'R')
		{
			// Increase RGB values that are not originally bigger than 0
			colour.g = (1 - (health / maxHealth)) * 0.5;
			colour.b = (1 - (health / maxHealth)) * 0.5;
		}
		else if (cClass == 'P')
		{
			colour.g = (1 - (health / maxHealth)) * 0.5;
		}
		else if (cClass == 'Y')
		{
			colour.b = (1 - (health / maxHealth)) * 0.5;
		}
		else if (cClass == 'G')
		{
			colour.r = (1 - (health / maxHealth)) * 0.5;
			colour.b = (1 - (health / maxHealth)) * 0.5;
		}
		else if (cClass == 'B')
		{
			colour.r = (1 - (health / maxHealth)) * 0.5;
			colour.g = (1 - (health / maxHealth)) * 0.5;
		}

		// Render the actor
		Gizmos::addBox(glm::vec3(position.x * square, position.y + (height / 2), position.z * square), glm::vec3(width, height, zwidth), true, colour, rotationMatrix);

		// If health drops to 0 or lower, the actor dies
		if (health <= 0)
		{
			dead = true;
		}

	}
}

void Actor::Draw(const glm::mat4 viewMatrix, const glm::mat4& a_projectionView)
{

}

// Returns bool if this actor is dead
bool Actor::GetDead()
{
	return dead;
}

// Returns this actor's position
glm::vec3 Actor::GetPos()
{
	return position;
}

// Returns actor's class
char Actor::GetClass()
{
	return cClass;
}
 
// Returns current health
float Actor::GetHealth()
{
	return health;
}

// Returns pointer to actor this actor is currently interacting with
Actor* Actor::GetInteractedActor()
{
	return interactedActor;
}

// Returns the vision range of this actor
float Actor::GetVisionRange()
{
	return visionRange;
}

// Returns this actor's current colour
glm::vec4 Actor::GetColour()
{
	return colour;
}

// Returns current behaviour of actor as an int
int Actor::GetBehaviour()
{
	switch (behaviour)
	{
	case WANDER:
		return 0;
		break;
	case FLEE:
		return 1;
		break;
	case CHASE:
		return 2;
		break;
	case FIGHT:
		return 3;
		break;
	}
}

// Sets the current behaviour of the actor
void Actor::SetBehaviour(int behNo)
{
	switch (behNo)
	{
	case 0:
		behaviour = WANDER;
		SetInteractedActor(NULL); // If wandering, this actor is not interacting with any other actor
		break;
	case 1:
		behaviour = FLEE;
		break;
	case 2:
		behaviour = CHASE;
		break;
	case 3:
		behaviour = FIGHT;
		break;
	}
}

// Set the actor that this actor is currently interacting with
void Actor::SetInteractedActor(Actor* otherActor)
{
	interactedActor = otherActor;
}
 
// Set current health of this actor
void Actor::SetHealth(float a_health)
{
	health = a_health;
}

// Set current colour of this actor
void Actor::SetColour(glm::vec4 color)
{
	colour = color;
}

// Checks if position is inside a wall
bool Actor::CheckInWall(glm::vec3 pos)
{
	float xPos = pos.x;
	float zPos = pos.z;

	// For all wall variables
	for (int i = 0; i < ((sizeof walls) / (sizeof *walls)); i++)
	{
		if ((xPos >= walls[i].x && xPos <= walls[i].z) && (zPos >= walls[i].y && zPos <= walls[i].w)) // If position is inside wall
		{
			return true;
		}		
	}

	return false;
}

// Chooses new speed and direction for actor
void Actor::NewVelocity()
{
	if (rand() % 2 <= 0)
	{
		alignment = -1;
	}
	else
	{
		alignment = 1;
	}

	velocity.x = alignment * (rand() % 5 + 2);

	if (rand() % 2 >= 1)
	{
		alignment = 1;
	}
	else
	{
		alignment = -1;
	}

	velocity.z = alignment * (rand() % 5 + 2);
 
}

// Checks for wall along a vector from this unit
bool Actor::CheckWallsBetween(glm::vec2 d_Pos)
{
	glm::vec3 vecBetween = glm::vec3(d_Pos.x, 0, d_Pos.y) - GetPos(); // Direction to other position

	float divAmount = sqrtf(powf(vecBetween.x, 2) + powf(vecBetween.z, 2));

	glm::vec3 normVecBetween = glm::vec3(vecBetween.x / divAmount, 0, vecBetween.z / divAmount); // Normalised vector direction

	// Check X and Z positions and move to other position until we know we are past it
	if (GetPos().x >= d_Pos.x && GetPos().z >= d_Pos.y) 
		// E.g. if this actor's position has higher X and Z values, move along vector until the position we are looking at has 
		// X and Z values less than or equal to the other position
	{
		for (glm::vec3 i = GetPos(); i.x >= d_Pos.x && i.z >= d_Pos.y; i += normVecBetween)
		{
			if (CheckInWall(i)) // Check wall of this location
			{
				return true;
			}
		}
	}
	else if (GetPos().x < d_Pos.x && GetPos().z >= d_Pos.y)
	{
		for (glm::vec3 i = GetPos(); i.x < d_Pos.x && i.z >= d_Pos.y; i += normVecBetween)
		{
			if (CheckInWall(i))
			{
				return true;
			}
		}
	}
	else if (GetPos().x >= d_Pos.x && GetPos().z < d_Pos.y)
	{
		for (glm::vec3 i = GetPos(); i.x >= d_Pos.x && i.z < d_Pos.y; i += normVecBetween)
		{
			if (CheckInWall(i))
			{
				return true;
			}
		}
	}
	else
	{
		for (glm::vec3 i = GetPos(); i.x < d_Pos.x && i.z < d_Pos.y; i += normVecBetween)
		{
			if (CheckInWall(i))
			{
				return true;
			}
		}
	}
	return false;
}

// Generalised checking if an actor is in vision range
bool Actor::A_CheckInRange(Actor* otherActor)
{
	float distance = sqrt(powf(GetPos().x - otherActor->GetPos().x, 2) + powf(GetPos().z - otherActor->GetPos().z, 2)); // Distance between two actors

	if (distance <= GetVisionRange()) // If actor is in range
	{
		if (otherActor == GetInteractedActor()) 
		{
			if (otherActor->GetDead()) // If actor is who we are interacting with and dead then reset to wandering
			{
				SetBehaviour(0);
				return false;
			}
			else // If not dead 
			{
				return !CheckWallsBetween(glm::vec2(otherActor->GetPos().x, otherActor->GetPos().y)); // Check for walls between this actor and other actor and return true if no walls
			}
		}
		else if (!otherActor->GetDead()) // If is not the actor we are interacting with and not dead
		{
			return !CheckWallsBetween(glm::vec2(otherActor->GetPos().x, otherActor->GetPos().y)); // Check for walls between this actor and other actor and return true if no walls
		}
		else // If other actor is dead
		{
			SetBehaviour(0);
			return false;
		}
	}
	else
	{
		if (otherActor == GetInteractedActor()) // If interacted actor is not in range then reset to wandering
		{
			SetBehaviour(0);
		}
		return false;
	}
}

// Rotates actor to face a direction
void Actor::RotateTo(glm::vec3 des)
{
	glm::vec3 vecTo = des; 

	// Case of rotation, what quadrant of circle it is in
	unsigned int r_Case = 0;

	// Change alignment of vector's positions
	if (vecTo.x < 0 && vecTo.z < 0)
	{
		vecTo.x *= -1;
		vecTo.z *= -1;
		r_Case = 4;
	}
	else if (vecTo.x < 0)
	{
		vecTo.x *= -1;
		r_Case = 3;
	}
	else if (vecTo.z < 0)
	{
		vecTo.z *= -1;
		r_Case = 2;
	}
	else
	{
		r_Case = 1;
	}

	float t_rotation = atan(vecTo.z / vecTo.x); // Find Tan^-1 of the Opposite/Adjacent lengths, this is the angle <90

	// For each quadrant add 90 for each quadrant passed
	switch (r_Case)
	{
		case 1:
			break;
		case 2:
			t_rotation += 270;
			break;
		case 3:
			t_rotation += 90;
			break;
		case 4:
			t_rotation += 180;
			break;
	}

	// Store rotation
	rotation = t_rotation;

	// Change variables in rotation matrix to match rotation
	rotationMatrix[0].x = cos(rotation);
	rotationMatrix[0].z = sin(rotation);
	rotationMatrix[2].x = -sin(rotation);
	rotationMatrix[2].z = cos(rotation);

}
