#include "Blue.h"

using namespace std;

Blue::Blue(char const* a_name)
{
	// Set class to blue with blue colour and set stats
	SetUp(a_name, 'B', glm::vec4(0, 0, 1, 1), 25.f, 2.f, 70.f, 1.1f);
}

Blue::~Blue()
{

}

void Blue::CheckInRange(Actor* otherActor)
{
	// If unit is not dead find other units in vision range that aren't dead
	if (!GetDead())
	{
		if (!otherActor->GetDead())
		{
			if (A_CheckInRange(otherActor))
			{
				InRange(otherActor); // Unit in range found
			}
			else if (otherActor == GetInteractedActor())
			{
				SetBehaviour(0); // Go back to wandering
			}
		}
		else if(otherActor == GetInteractedActor())
		{
			SetBehaviour(0); // Go back to wandering
		}
	}
}

// If unit is in vision range
void Blue::InRange(Actor* otherActor)
{
	if (otherActor == GetInteractedActor()) // If the actor is the one this unit currently interacting with
	{
		return;
	}
	else if (GetInteractedActor() == NULL && GetBehaviour() == 0) // If this unit is not interacting with another and is wandering
	{
		if (!otherActor->GetDead())
		{
			SetInteractedActor(otherActor); // Set the actor we are interacting with
			switch (GetBehaviour())
			{
			case 0: //WANDER
				if (otherActor->GetBehaviour() == 4 || otherActor->GetBehaviour() == 2) // If the actor is chasing or fighting another actor
				{
					char otherClass = otherActor->GetClass(); // The class of the actor
					char otherFClass = otherActor->GetInteractedActor()->GetClass(); // The class of the actor that the actor is fighting/chasing

					if (otherClass == 'R') // Always chase Red units
					{
						SetBehaviour(2);
					}
					else if (otherClass == 'Y') // Only fight Yellow units if they are fighting other Yellow units
					{
						if (otherFClass != 'Y')
						{
							SetInteractedActor(otherActor->GetInteractedActor());
							SetBehaviour(2);
						}
						break;
					}
					else if (otherClass == 'P') // Always fight Purple units unless they are fighting Red units
					{
						if (otherFClass == 'R')
						{
							SetInteractedActor(otherActor->GetInteractedActor());
							SetBehaviour(2);
						}
						else 
						{
							SetBehaviour(2);
						}
					}
					else if (otherClass == 'G') // Don't fight Green units if they are fighting Red or Purple units
					{
						if (otherFClass == 'P' || otherFClass == 'R')
						{
							SetInteractedActor(otherActor->GetInteractedActor());
							SetBehaviour(2);
							break;
						}
						else
						{
							SetBehaviour(2);
						}
					}
					else // Always fight those a blue unit is fighting
					{
						SetInteractedActor(otherActor->GetInteractedActor());
						SetBehaviour(2);
					}
					
				}
				break;
			case 1: //FLEE


				break;
			case 2: //CHASE


				break;
			case 3: //FIGHT


				break;
			}
		}
	}
}