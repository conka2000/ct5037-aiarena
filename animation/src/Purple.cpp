#include "Purple.h"

using namespace std;

Purple::Purple(char const* a_name)
{
	// Set class to purple with purple colour and set stats with very low vision range
	SetUp(a_name, 'P', glm::vec4(1, 0, 1, 1), 8.f, 3.f, 60.f, 1.2f);
}

Purple::~Purple()
{

}

void Purple::CheckInRange(Actor* otherActor)
{
	// If unit is not dead find other units in vision range that aren't dead
	if (!GetDead())
	{
		if (!otherActor->GetDead())
		{
			if (A_CheckInRange(otherActor))
			{
				InRange(otherActor); // Unit in range found
			}
			else if (otherActor == GetInteractedActor())
			{
				SetBehaviour(0); // Go back to wandering
			}
		}
		else if (otherActor == GetInteractedActor())
		{
			SetBehaviour(0); // Go back to wandering
		}
	}
}

// If unit is in vision range
void Purple::InRange(Actor* otherActor)
{
	if (otherActor == GetInteractedActor()) // If the actor is the one this unit currently interacting with
	{
		return;
	}
	else if (GetInteractedActor() == NULL && GetBehaviour() == 0) // If this unit is not interacting with another and is wandering
	{
		if (!otherActor->GetDead())
		{
			SetInteractedActor(otherActor); // Set the actor we are interacting with
			switch (GetBehaviour())
			{
			case 0: //WANDER
				if (otherActor->GetClass() != 'P') // Chase the unit if it is not another Purple unit
				{
					SetBehaviour(2);
				}
				break;
			case 1: //FLEE


				break;
			case 2: //CHASE


				break;
			case 3: //FIGHT


				break;
			}
		}
	}
}