#ifndef __Purple_H_
#define __Purple_H_

#include "Actor.h"
//Purple is the assassin class, it will attack others at close range except for other purple units
class Purple : public Actor
{
public:
	Purple(char const* a_name);
	~Purple();

	void CheckInRange(Actor* otherActor);
private:
	void InRange(Actor* otherActor);
};

#endif //__Purple_H_