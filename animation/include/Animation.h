#ifndef __Animation_H_
#define __Animation_H_

#include "FBXFile.h"
#include <glm/glm.hpp>

#include "Application.h"
#include "Stage.h"
#include "Actor.h"

#include "Red.h"
#include "Yellow.h"
#include "Purple.h"
#include "Green.h"
#include "Blue.h"

// s1504719 - Cameron Seeley

// Derived application class that wraps up all globals neatly
class Animation : public Application
{
public:

	Animation();
	virtual ~Animation();

protected:

	virtual bool onCreate();
	virtual void Update(float a_deltaTime);
	virtual void Draw();
	virtual void Destroy();

	glm::mat4	m_cameraMatrix;
	glm::mat4	m_projectionMatrix;
	glm::mat4	m_modelMatrix;
	glm::mat4	m_modelMatrix2;
	bool moveUp = true;
	
	unsigned int	m_programID;
	unsigned int	m_vertexShader;
	unsigned int	m_fragmentShader;

	unsigned int m_maxIndices;
	unsigned int m_vao;
	unsigned int m_vbo;
	unsigned int m_ibo;

	//Texture Handle for OpenGL
	unsigned int m_textureID;

	//FBX Model File
	FBXFile* m_fbxModel;
	FBXFile* m_fbxModel2;
	
	// Stage manager
	Stage stage;

	// Actors
	Red* red1;
	Red* red2;
	Red* red3;
	Red* red4;

	Yellow* yellow1;

	Purple* purple1;
	Purple* purple2;

	Green* green1;
	Green* green2;

	Blue* blue1;
	Blue* blue2;
};

#endif // __Application_H_