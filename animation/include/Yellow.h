#ifndef __Yellow_H_
#define __Yellow_H_

#include "Actor.h"
// Yellow is the coward class, it will run from all perceived threats, will only fight if it can't escape
class Yellow : public Actor
{
public:
	Yellow(char const* a_name);
	~Yellow();

	void CheckInRange(Actor* otherActor);
private:
	void InRange(Actor* otherActor);
};

#endif //__Yellow_H_