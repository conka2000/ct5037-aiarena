#ifndef __Red_H_
#define __Red_H_

#include "Actor.h"
// Red is the aggressive class, it will attack all other units
class Red : public Actor
{
public:
	Red(char const* a_name);
	~Red();

	void CheckInRange(Actor* otherActor);
private:
	void InRange(Actor* otherActor);
};

#endif //__Red_H_