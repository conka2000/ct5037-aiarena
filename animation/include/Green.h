#ifndef __Green_H_
#define __Green_H_

#include "Actor.h"
// Green is the neutral class, it will attack assassins (purple) and run from aggressive units (red)
class Green : public Actor
{
public:
	Green(char const* a_name);
	~Green();

	void CheckInRange(Actor* otherActor);
private:
	void InRange(Actor* otherActor);
};

#endif //__Green_H_