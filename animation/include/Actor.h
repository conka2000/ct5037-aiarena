#ifndef __Actor_H_
#define __Actor_H_

#include "Gizmos.h"
#include "Utilities.h"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/ext.hpp>
#include <iostream>
// This is the class for all moveable units with AI
class Actor
{
public:
	Actor();
	~Actor();

	void SetUp(char const* a_name, char cType, glm::vec4 color, float vision, float attack, float a_health, float a_speed);
	void Start(float a_seed);
	void Update(float a_deltaTime);
	void Draw(const glm::mat4 viewMatrix, const glm::mat4& a_projectionView);

	glm::vec3 GetPos();
	char GetClass();
	Actor* GetInteractedActor();
	int GetBehaviour();
	float GetVisionRange();
	glm::vec4 GetColour();
	float GetHealth();
	bool GetDead();

private:
	void NewVelocity();
	void RotateTo(glm::vec3 des);
	bool CheckWallsBetween(glm::vec2 d_Pos);

	// Name of specific unit
	char const* name = "N/A";
	// Class of unit
	char cClass;

	// Whether unit has died
	bool dead = false;

	// Size
	float width = 0.2f;
	float height = 0.4f;
	float zwidth = 0.2f;
	
	// Speed of unit movement when not wandering
	float speed = 1;
	// Current position of unit and its velocity
	glm::vec3 position = glm::vec3(0, 0, 0);
	glm::vec3 velocity = glm::vec3(0, 0, 0);
	// Rotation of unit
	float rotation = 0.f;
	// Matrix to hold rotation for rendering
	glm::mat4 rotationMatrix 
	{
		0,0,0,0,
		0,1,0,0,
		0,0,0,0,
		0,0,0,1
	};

	// Boundaries of arena
	float xbound = 19;
	float zbound = 19;
	// Enum holding current behaviour
	enum Behaviour{ WANDER, FLEE, CHASE, FIGHT };
	Behaviour behaviour = WANDER;

	// For random velocities to be able to go in positive and negative directions
	int alignment = 1;

	// Range of interacting with other units
	float visionRange = 15.f;
	// The other actor that this unit is interacting with currently
	Actor* interactedActor;
	// Holder for vectors between two points
	glm::vec3 vecBetween;

	// Timer holding allowed of time to wander
	float wanderTimer = 0.f;
	float allowedWanderTime;

	// Maximun health
	float maxHealth = 100;
	// Current health
	float health = 100;
	// Power to bring down other's health
	float attackPower = 1;

	// Wall boundaries
	glm::vec4 walls[8];
	// Turn node positions
	glm::vec2 tNodes[23];
	// Node currently moving to if moving to a node
	glm::vec2 currentNode;
	// Node last moved to
	glm::vec2 lastNode;
	// If moving to node
	bool nodeSet = false;

	// Colour of unit to be rendered
	glm::vec4 colour = glm::vec4(1, 1, 1, 1);

	// Seed to dictate random variables
	float seed = time(NULL);
	// Size of grid square in arena 
	float square = 0.5f;

protected:

	bool CheckInWall(glm::vec3 pos);
	bool A_CheckInRange(Actor* otherActor);

	void SetColour(glm::vec4 color);
	void SetBehaviour(int behNo);
	void SetInteractedActor(Actor* otherActor);
	void SetHealth(float a_health);
};

#endif 
