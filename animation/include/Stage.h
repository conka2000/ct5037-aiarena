#ifndef __Stage_H_
#define __Stage_H_

#include "Gizmos.h"
#include "Utilities.h"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/ext.hpp>
#include <iostream>
// This class is responsible for rendering the arena walls and the turn nodes
class Stage
{
public:
	Stage();
	~Stage();

	void Update(float a_deltaTime);
	void Draw(const glm::mat4 viewMatrix, const glm::mat4& a_projectionView);
private:
	float square = 0.5f;

	// Walls are dark grey and turn nodes are light grey
	glm::vec4 wallColour = glm::vec4(0.5, 0.5, 0.5, 1); 
	glm::vec4 nodeColour = glm::vec4(0.75, 0.75, 0.75, 1);

};

#endif 

