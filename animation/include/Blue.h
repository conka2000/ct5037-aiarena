#ifndef __Blue_H_
#define __Blue_H_

#include "Actor.h"
// Blue is the protector class, it will only fight those who are fighting others and will attack who it seems as the least innocent
class Blue : public Actor
{
public:
	Blue(char const* a_name);
	~Blue();

	void CheckInRange(Actor* otherActor);
private:
	void InRange(Actor* otherActor);
};

#endif //__Blue_H_